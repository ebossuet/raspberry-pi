<?php

namespace Controllers;

use Models\Item;
use Models\Interfaces\PlayerInterface;
use Util\AjaxUtil;

if (!defined('FIFO')) define('FIFO', '/var/www/fifo');

/**
 * Controller used as a base for specific media controllers
 *
 * @author Etienne Bossuet <etienne.bossuet@etu-u-bordeaux1.fr>
 * @author Charlie Maugin <charlie.maugin@etu.u-bordeaux1.fr>
 * @author Hugo Quezada Serres <hugo.quezada--serres@etu.u-bordeaux1.fr>
 */
class Controller
{

    const PAUSE = 'p';
    const QUIT = 'q';
    const PREVIOUS = 'p';
    const NEXT = 'n';
    const RESTART = 'i';

    // , 
    // video , 
    //image 
    private static $status = 0;
    private static $_musicExtFilter = array("mp3", "wma", "wav", "ogg", "flac");
    private static $_imageExtFilter = array("jpeg", "ppm", "gif", "tiff", "xwd", "bmp", "png", "jpg");
    private static $_videoExtFilter = array("avi", "mp4", "mkv", "mpg", "wmv", "3gp", "mov", "vob", "mpeg");

    /**
     * Method used to play or display the Item given in parameter.
     * Interupt the current Item if it is not idle
     * @param Item $i Item to be played or displayed by the Raspberry
     */
    public function play(PlayerInterface $item,$type)
    {
		if($type=='video')
		$app='omxplayer';
		else
		$app='fim';
		$playlist = explode(",",$item->getPath());
		if(is_array($playlist) == 1){
			foreach($playlist as $file){
				$list .= $file.' ';
			}
		}
		else{
		$list = $playlist;}
		
        exec('pgrep '.$app, $pids);  //omxplayer
        if (empty($pids))
        {
            @unlink(FIFO);
            posix_mkfifo(FIFO, 0777);
            chmod(FIFO, 0777);
           	shell_exec(getcwd() . '/scriptshell/'.$app.'_php.sh ' . $list);
            //$out = 'playing ' . basename($file);
        }
    }

    /**
     * Method used to stop an item curently being played.
     */
    public function stop(){
		shell_exec('sudo '.getcwd() . '/scriptshell/kill.sh ');
	}

    /**
     * Method used to pause an item curently being played.
     */
    public function pause($type){
		if($type=='image'){
			exec('pgrep timer.sh', $pids);
			if (!empty($pids))
			shell_exec('sudo '.getcwd() . '/scriptshell/killtimer.sh ');
			else
			shell_exec('sudo '.getcwd() . '/scriptshell/timer.sh ');
		}
		else{
			$this->send(self::PAUSE);
		}
	}

    /**
     * Select
     */
    public function next($type){
		if($type=='image'){
			$this->send(self::NEXT);
		}
		else{
			 $this->send(self::QUIT);
		}
	}

    /**
     * Method used to pause an item curently being played.
     */
    public function previous($type){
		if($type=='image'){
			$this->send(self::PREVIOUS);
		}
		else{
			$this->send(self::RESTART);
		}
	}
	
	
	private function send($command)
    {
        exec('pgrep fim', $fimpids);
        exec('pgrep omxplayer', $omxpids);
        if (!empty($omxpids) || !empty($fimpids))
        {
            if (is_writable(FIFO))
            {
                if ($fifo = fopen(FIFO, 'w'))
                {
                    stream_set_blocking($fifo, false);
                    fwrite($fifo, $command);
                    fclose($fifo);
                }
            }
        }
    }
	

    public static function getFiles($path)
    {
        $result = Controller::getDirectoryList($path);
        AjaxUtil::send();
    }

    /**
     * make a list of file present in the given directory
     * @param String $directory directory to explore
     * @return array an array with the format [index]=>["name"]["extension"]["taille"]["path"]
     */
    public static function getDirectoryList($path)
    {
        $target = getcwd() . DIRECTORY_SEPARATOR . $path;
        $directoryName = basename(realpath($target));
        //Si le dossier est bien ouvert (opendir() retourne un pointeur sur un dossier. Il est affecté à $dossier. Faire un if permet de vérifier que le dossier a bien été ouvert.
        if (($directory = opendir($target)) && $directoryName != "www")
        {
            //Pour chacun des fichiers du dossier ( = Tant qu'il y en a, on récupère les fichiers un par un )
            while (false !== ($fichier = readdir($directory)))
            {
                $fullPath = $target . DIRECTORY_SEPARATOR . $fichier;
                $relativePath = $path . DIRECTORY_SEPARATOR . $fichier;
                //AjaxUtil::addParam($relativePath);
                //Si ce n'est pas un dossier
                if (!is_dir($fullPath))
                {
                    //Extension du fichier
                    $extension = explode(".", $fichier);
                    $extension = $extension[count($extension) - 1]; //Le dernier élément est l'extension
                    //
                    $tabName = explode(DIRECTORY_SEPARATOR, $path);
                    $tabName = $tabName[0];
                    //Taille du fichier en ko
                    $octets = filesize($path . DIRECTORY_SEPARATOR . $fichier);
                    $taille = $octets / 1024;
                    $taille = round($taille, 2);

                    //On ajoute le nom du fichier, son extension et sa taille dans le tableau $tab
                    if (($tabName === "music" && in_array(strtolower($extension), Controller::$_musicExtFilter)) ||
                            ($tabName == "image" && in_array(strtolower($extension), Controller::$_imageExtFilter)) ||
                            ($tabName == "video" && in_array(strtolower($extension), Controller::$_videoExtFilter)))
                    {
                        AjaxUtil::addParam(array(
                            "name" => $fichier,
                            "extension" => $extension,
                            "taille" => $taille,
                            "path" => $relativePath,
                            "isDirectory" => false));
                    }
                } else
                {
                    // check you can't explore above the root directory
                    if ((basename($fichier) !== '..' || !($directoryName === "music" || $directoryName === "image" || $directoryName === "video" )) && (basename($fichier) !== '.' ))
                    {
                        AjaxUtil::addParam(array(
                            "name" => $fichier,
                            "isDirectory" => true,
                            "path" => $relativePath));
                    }
                }
            }

            //On libère les ressources utilisées pour l'ouverture du dossier
            closedir($directory);
        } else
        {
            
        }
    }

    public static function searchFiles($type, $path)
    {
        //fonction find et generation tableau 
        $command = "./scriptshell/find.sh " . $type . " " . $path;
        shell_exec($command);

        $handle = fopen("result", "r");
        while (!feof($handle))
        {
            $vids[] = rtrim(fgets($handle, 4096));
        }

        foreach ($vids as $val)
        {
            if ($val != false)
            {
                $array = array('path' => $val, 'name' => basename($val), 'isDirectory' => false);
                AjaxUtil::addParam($array);
            }
        }
        AjaxUtil::send();
    }

    public static function mountFolder($type, $path)
    {
        $command = "./scriptshell/mount.sh " . $_SERVER["REMOTE_ADDR"] . " " . $path . " " . $type;
        shell_exec('sudo ' . $command);
        Controller::getFiles($type . "/Shared/" . $path);
    }
    
    public static function stream($path){
        $command = "./scriptshell/stream.sh ".$path;
        
        shell_exec($command);
    }
    
    public static function stream_getTitle($path){
        $command = "./scriptshell/gettitle.sh ".$path;
        //-e pour print le titre de la video et le récupérer dans un fichier comme find.sh
        shell_exec($command);
        
        $handle = fopen("title", "r");
        while (!feof($handle))
        {
            $title = rtrim(fgets($handle, 4096));
            AjaxUtil::addParam(array('path' => $path, 'name' => basename($title), 'isDirectory' => false));
        }
        AjaxUtil::send();
    }
}

?>
