<?php
function __autoload($class)
{
    static $classDir = './class';
    $file = str_replace('\\', DIRECTORY_SEPARATOR, ltrim($class, '\\')) . '.class.php';
    require "$classDir/$file";
}
?>
