<!DOCTYPE html>
<html>
    <head>
        <title>Raspberry player</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="ressources/styles/styles.css" />
        <link rel="stylesheet" type="text/css" href="ressources/styles/styles_768.css" media="screen and (max-width: 1023px)"/>
        <link rel="stylesheet" type="text/css" href="ressources/styles/styles_480.css" media="screen and (max-width: 767px)"/>
        <link rel="stylesheet" type="text/css" href="ressources/styles/styles_320.css" media="screen and (max-width: 479px)"/>

        <script src="ressources/scripts/js/jquery.js"></script>
        <script src="ressources/scripts/js/interface.js"></script>
        <script src="ressources/scripts/js/ajax.js"></script>
    </head>
    <body>
        <div id="tip">Show remote</div>
        <div id="menu">
            <form>
                <input type="button" value="MUSIC" id="btn_music" class="btn_menu">
                <input type="button" value="IMAGE" id="btn_image" class="btn_menu">
                <input type="button" value="VIDEO" id="btn_video" class="btn_menu">
                <input type="hidden" value="" id="page_type">
            </form>
        </div>
        <div id="file_menu">
            <div id="switchR">
                <form>
                    <input type="button" title="Show remote" id="btn_switchR">
                </form>
            </div>
            <div id="file_container">
                <div class="list_head">
                    <table class="head">                
                        <thead>
                            <tr><th>Files list<label for="q"></label><input type="text" name="q" id="q" placeholder="Search..."></th></tr>
                            <tr><th id="toolbar">
                                    <input type="button" id="btn_back" title="back"><span class="separator first">|</span> 
                                    <input type="text" name="importField" id="importField" title="Write the folder's name to share" placeholder="Folder to share...">
                                    <input type="button" id="btn_import" title="Import your local files !" class="btn_specific" value="Import"><span class="separator second">|</span>
                                    <input type="text" name="ytField" id="ytField" title="Copy the link of the video to stream" placeholder="Link to stream...">
                                    <input type="button" id="btn_yt" title="Stream your video !" class="btn_specific" value="Stream">
                                    <input type="button" value="ADD ALL" title="Add all folder" id="btn_addAll" class="btn_specific">
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="list_container">
                    <table class="list">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="form_specific">
                        <div id="btn_playAll"><input type="button" value="PLAY ALL" class="btn_specific"></div>
                        <div id="btn_shuffleAll"><input type="button" value="SHUFFLE ALL" class="btn_specific"></div>
                </div>
            </div>
            <div class="playlist_container" id="playlist_music">
                <div class="list_head">
                    <table class="head">                
                        <thead>
                            <tr>
                                <th>Playlist<input type="button" value="EMPTY" class="btn_specific"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="list_container">
                    <table class="list">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="form_specific">
                    <div class="btn_listnormal"><input type="button" value="PLAY LIST" class="btn_specific" id="btn_playListMusic"></div>
                    <div class="btn_listshuffle"><input type="button" value="SHUFFLE LIST" class="btn_specific"></div>
                </div>
            </div>
            <div class="playlist_container" id="playlist_image">
                <div class="list_head">
                    <table class="head">                
                        <thead>
                            <tr>
                                <th>Playlist<input type="button" value="EMPTY" class="btn_specific"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="list_container">
                    <table class="list">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="form_specific">
                    <div class="btn_listnormal"><input type="button" value="WATCH THESE IMAGES" class="btn_specific" id="btn_playListImage"></div>
                    <div class="btn_listshuffle"><input type="button" value="SHUFFLE LIST" class="btn_specific"></div>
                </div>          
            </div>
            <div class="playlist_container" id="playlist_video">
                <div class="list_head">
                    <table class="head">                
                        <thead>
                            <tr>
                                <th>Playlist<input type="button" value="EMPTY" class="btn_specific"></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="list_container">
                    <table class="list">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="form_specific">
                    <div class="btn_listnormal"><input type="button" value="WATCH THESE VIDEOS" class="btn_specific" id="btn_playListVideo"></div>
                    <div class="btn_listshuffle"><input type="button" value="SHUFFLE LIST" class="btn_specific"></div>
                </div> 
            </div>
        </div>
        <div id="remote_menu">
            <div id="switchL">
                <form>
                <input type="button" title="Show files" id="btn_switchL">
                </form>
            </div>
            <div class="playlist_container">
                <div class="list_head">
                    <table class="head">                
                        <thead>
                            <tr><th>Playlist</th>
                        </tr></thead>
                    </table>
                </div>
                <div class="list_container">
                    <table class="list">
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div id="remote_bar">
                <form>
                    <input type="button" title="Previous" id="remoteBtn_previous">
                    <input type="button" title="Play/Pause" class="remoteBtn_play" id="remoteBtn_playpause">
                    <input type="button" title="Stop" id="remoteBtn_stop">
                    <input type="button" title="Next" id="remoteBtn_next">
                </form>
            </div>
        </div>
    </body>
</html>
