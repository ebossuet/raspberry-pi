#!/bin/bash

check_process() {
  [ `pgrep -n $1` ] && return 1 || return 0
}

waitForCommand(){
 echo "running..."
}

playNextVideo(){
# echo "not running... "
 echo "play $1"
 ./omx_php.sh "$1"
 break
}

for arg in "$@"
do
 echo $arg
 while [ 1 ]; do 
  sleep 1
  check_process "omxplayer"
  [ $? -eq 0 ] && playNextVideo "$arg"
  [ $? -eq 1 ] && waitForCommand
 done
done
