<?php

namespace Models\Interfaces;

use Models\Interfaces\PlayerInterface;

/**
 * Interface for all object readable by FBI Image reader
 * @author Etienne Bossuet <etienne.bossuet@etu-u-bordeaux1.fr>
 * @author Charlie Maugin <charlie.maugin@etu.u-bordeaux1.fr>
 * @author Hugo Quezada Serres <hugo.quezada--serres@etu.u-bordeaux1.fr>
 */
interface FBIPlayable extends PlayerInterface
{
    /**
     * get the absolute path to the file
     * @return String An absolute path to a file
     */
    public function getPath();
}

?>
