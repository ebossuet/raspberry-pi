#!/bin/bash

check_process() {
  [ `pgrep -n $1` ] && return 1 || return 0
}

waitForCommand(){
 echo "$1 is running..."
 sudo killall "$1"
}

check_process "playlist.sh"
[ $? -eq 1 ] && waitForCommand "playlist.sh"

check_process "omxplayer.bin"
[ $? -eq 1 ] && waitForCommand "omxplayer.bin"

check_process "timer.sh"
[ $? -eq 1 ] && waitForCommand "timer.sh"

check_process "fim"
[ $? -eq 1 ] && waitForCommand "fim"

check_process "fbi"
[ $? -eq 1 ] && waitForCommand "fbi"

check_process "omx_php.sh"
[ $? -eq 1 ] && waitForCommand "omx_php.sh"

sudo -u pi xrefresh -d :0
