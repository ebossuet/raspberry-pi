<?php
namespace Models\Interfaces;

/**
 * @author Etienne Bossuet <etienne.bossuet@etu-u-bordeaux1.fr>
 * @author Charlie Maugin <charlie.maugin@etu.u-bordeaux1.fr>
 * @author Hugo Quezada Serres <hugo.quezada--serres@etu.u-bordeaux1.fr>
 */
/**
 * Common interface for all object readable by a player on the Raspberry
 */
interface PlayerInterface
{
}

?>
