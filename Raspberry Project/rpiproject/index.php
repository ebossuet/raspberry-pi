<?php

require "./autoload.php";

use Controllers\Controller;
use Models\Item;

/*
 *  Portions Copyrighted 2011 Sun Microsystems, Inc.
 */

/**
 * Index page which initialize the user interface to dispay to the user
 * @author Etienne Bossuet <etienne.bossuet@etu-u-bordeaux1.fr>
 * @author Charlie Maugin <charlie.maugin@etu.u-bordeaux1.fr>
 * @author Hugo Quezada Serres <hugo.quezada--serres@etu.u-bordeaux1.fr>
 */
class index
{

    private $_Controller;

    /**
     * @var index Singleton's instance
     * @access private
     * @static
     */
    private static $_instance = null;

    /**
     * The constructor log the user in and set controllers up.
     *
     * @param void
     * @return void
     */
    private function __construct()
    {
        $this->_Controller = new Controller();
    }

    /**
     * Method which create the unique instance of this class
     * if it not already exist
     *
     * @param void
     * @return Singleton
     */
    public static function getInstance()
    {

        if (is_null(self::$_instance))
        {
            self::$_instance = new index();
        }

        return self::$_instance;
    }

    /**
     * Execute and process the ajax request
     */
    public function doRequest(&$request)
    {

        $type = $request['json']['type'];
        $path = $request['json']['path'];
        $command = $request['json']['command'];
		

        switch ($command)
        {
            case "play":
                    $this->_Controller->play(new Item($path),$type);
                break;

            case "pause":
                    $this->_Controller->pause($type);
                break;

            case "stop":
                    $this->_Controller->stop($type);
                break;
            case "previous":
                    $this->_Controller->previous($type);
                break;
            case "next":
                    $this->_Controller->next($type);
                break;

            case "scan":
                    Controller::getFiles($path);
                break;
                
            case "search":
                Controller::searchFiles($type, $path);
                break;
            
            case "mount":
                Controller::mountFolder($type, $path);
                break;
            case "stream":
                $this->_Controller->stream($path);
                break;
            case "stream_getTitle":
                $this->_Controller->stream_getTitle($path);
                break;

            default :
                echo "commande inconnue...";
                break;
        }
    }

    /**
     * fetch the client interface web page and
     */
    public function fetchUI()
    {
        include("./pages/index.php");
    }

}

////////////////////////////////////////////////////////////////////////////////
//session_start();
//if (!isset($_SESSION['SID']) || $_SESSION['SID'] == session_id()) {
$index = index::getInstance();

/* AJAX check  */
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
    $index->doRequest($_POST);
} else
{
    $index->fetchUI();
}
?>
