#!/bin/bash

echo "Mis a jour complete et installation du projet raspberry pi"
echo "-----------------------------------"
echo "-----------------------------------"
echo "Attention, ce script prend du temps, du fait des nombreux paquets a installer"
echo "ne pas couper la connexion a internet pendant l'execution"
echo "appuyez sur enter pour continuer"
read
echo "update des fichiers"
sudo apt-get update
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "update des paquets"
sudo apt-get upgrade
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "installation de git"
echo "sudo apt-get install git-core"
sudo apt-get install git-core
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "sudo apt-get install ntpdate"
sudo apt-get install ntpdate
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "sudo ntpdate -u ntp.ubuntu.com"
sudo ntpdate -u ntp.ubuntu.com
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "sudo apt-get install ca-certificates"
sudo apt-get install ca-certificates
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "installation de apache et php"
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
sudo apt-get install apache2
sudo apt-get install php5
sudo apt-get install libapache2-mod-php5
sudo apt-get install php5-gd
sudo apt-get install php5-curl
sudo apt-get install php5-xmlrpc
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "installation de samba"
sudo ./simplesamba.sh
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "installation de omxplayer"
sudo apt-get install omxplayer
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "installation de fim"
sudo apt-get install fim
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "copie des fichiers du projet"
sudo cp -R rpiproject/* /var/www/
sudo chown -R pi /var/www/*
sudo rm /var/www/index.html
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "installation du script de suppressio des rep reseaux"
sudo cp rmdir /etc/init.d/
sudo ln -s /etc/init.d/rmdir /etc/rc2.d/S03rmdir
sudo ln -s /etc/init.d/rmdir /etc/rc3.d/S03rmdir
sudo ln -s /etc/init.d/rmdir /etc/rc4.d/S03rmdir
sudo ln -s /etc/init.d/rmdir /etc/rc5.d/S03rmdir
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "A l'aide de la commande 'sudo visudo' , ajoutez la ligne suivante � la fin du fichier: "
echo "www-data ALL=(ALL:ALL) NOPASSWD: /var/www/scriptshell/*.sh"
echo "appuyez sur enter pour continuer"
read
echo "finalisation de l'installation"
sudo apt-get install x11-xserver-utils
sudo usermod -a -G video www-data
sudo chmod -R 777 /var/www
sudo usermod -a -G video www-data
echo "update du firmware"
echo "Pour plus d'info : https://github.com/Hexxeh/rpi-update/blob/master/README.md"
echo "telechargement du script d'update"
echo "sudo wget http://goo.gl/1BOfJ -O /usr/bin/rpi-update && sudo chmod +x /usr/bin/rpi-update"
sudo wget http://goo.gl/1BOfJ -O /usr/bin/rpi-update && sudo chmod +x /usr/bin/rpi-update
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "sudo rpi-update"
sudo rpi-update
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
echo "installation terminee"
echo "-----------------------------------"
echo "-----------------------------------"
echo "-----------------------------------"
echo "appuyez sur enter pour continuer"
read
