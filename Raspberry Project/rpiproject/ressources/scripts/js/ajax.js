//Override Jquerry to include a postJSON
jQuery.extend({
    postJSON: function( url, json, callback) {
        $.ajax({
            "type":"POST",
            "url": url,
            "data": json
        }).always(callback);
    }
});

// function which send AJAX request to the server
function sendAjax(type,command,path,callback)
{
    //alert(encodeURIComponent(path));
    var json_request = {
        json :{
            "type" : type,
            "path" : unescape(path),
            "command" : command
        }
    };
    
    if(typeof callback !== "function")
        $.postJSON("index.php", json_request);
    else
        $.postJSON("index.php", json_request,callback);    
}

function goplaylist(type,list)
{
    sendAjax(type, "play", list, null);
}

function play(path)
{
    var address = new RegExp("\/var\/www/");
    var filename = new RegExp("\/.*");
    var step1 = path.toString().replace(address, "");
    var step2 = step1.toString().replace(filename, "");
    
    sendAjax(step2, "play", path);
    
    $("#remoteBtn_playpause").css("background-image","url('ressources/imgs/remoteBtn_pause.png')");
    $("#remoteBtn_playpause").attr("class","remoteBtn_pause");
}

function send(type, command)
{
    if(type == "music" || type== "video")
        sendAjax("multimedia", command, null);
    else
        sendAjax("image", command, null);
}

function getFiles(path){
    
    sendAjax(null, "scan", path, displayFiles);
}

function searchFiles(type, q){
    sendAjax(type, "search", q, displayFiles);
}
function mountFolder(type, folder){
    sendAjax(type, "mount", folder, displayFiles);
    $("#importField").val('');
}

function stream(link){
    sendAjax("video", "stream_getTitle", link, yt_switchR);
    sendAjax("video", "stream", link);
}


