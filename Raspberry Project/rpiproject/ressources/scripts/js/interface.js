//Music/Video/Image buttons
$(function(){
    var search_bar = $("#q");
    var yt_block = $("#ytField,#btn_yt,.second");
    var page_type = $("#page_type").val();
    var playlist_image = $("#file_menu>#playlist_image");
    var playlist_music = $("#file_menu>#playlist_music");
    var playlist_video = $("#file_menu>#playlist_video");
    var nb_imageFiles, nb_musicFiles, nb_videoFiles = "";
    var table_playlistRemote = $("#remote_menu>.playlist_container>.list_container>table.list>tbody");
    var row, row_tmp, file_name, table_temp, btn_watchAll, btn_playAll = "";
    
    $("#btn_music").click(function(){
        nb_musicFiles = $("#file_menu>#playlist_music>.list_container>table.list>tbody>tr").length;
        table_playlistRemote.empty();
        playlist_music.css("display","none");
        if(nb_musicFiles>0){
            playlist_music.css("display","block");
        }
        playlist_video.css("display","none");
        playlist_image.css("display","none");
        
        yt_block.css("display", "none");
        
        $("#remote_menu").css("display","none");
        $("#file_menu").css("display","block");
        $("#remote_menu").css("float","right");
        $("#file_menu").css("float","left");
        
        btn_playAll = '<input class="btn_specific" id="btn_playAll" type="button" value="PLAY ALL">';
        $("#btn_playAll>input.btn_specific").replaceWith(btn_playAll);
        
        $("#page_type").attr("value","music");
        search_bar.val('');
        generate_btnInteraction();
        generate_switches();
        generate_tips();
        $(".btn_menu").removeClass("active");
        $(this).addClass("active");
        page_type = $("#page_type").val();
        generate_specific(page_type);
        getFiles('music');
    });
    
    $("#btn_image").click(function(){
        nb_imageFiles = $("#file_menu>#playlist_image>.list_container>table.list>tbody>tr").length;
        table_playlistRemote.empty();
        playlist_image.css("display","none");
        if(nb_imageFiles>0){
            playlist_image.css("display","block");
        }
        playlist_video.css("display","none");
        playlist_music.css("display","none");
        
        $("#remote_menu").css("display","none");
        $("#file_menu").css("display","block");
        $("#remote_menu").css("float","right");
        $("#file_menu").css("float","left");
        
        yt_block.css("display", "none");
        
        btn_watchAll = '<input class="btn_specific" id="btn_watchAll" type="button" value="WATCH ALL">';
        $("#btn_playAll>input.btn_specific").replaceWith(btn_watchAll);
        
        $("#page_type").attr("value","image");
        search_bar.val('');
        generate_btnInteraction();
        generate_switches();
        generate_tips();
        $(".btn_menu").removeClass("active");
        $(this).addClass("active");
        page_type = $("#page_type").val();
        generate_specific(page_type);
        getFiles('image');
    });
    $("#btn_video").click(function(){
        nb_videoFiles = $("#file_menu>#playlist_video>.list_container>table.list>tbody>tr").length;
        table_playlistRemote.empty();
        playlist_video.css("display","none");
        if(nb_videoFiles>0){
            playlist_video.css("display","block");
        }
        playlist_music.css("display","none");
        playlist_image.css("display","none");
        $("#remote_menu").css("float","right");
        $("#file_menu").css("float","left");
        
        $("#remote_menu").css("display","none");
        $("#file_menu").css("display","block");
        
        yt_block.css("display", "block");
        
        btn_watchAll = '<input class="btn_specific" id="btn_watchAll" type="button" value="WATCH ALL">';
        $("#btn_playAll>input.btn_specific").replaceWith(btn_watchAll);
        
        $("#page_type").attr("value","video");
        search_bar.val('');
        generate_btnInteraction();
        generate_switches();
        generate_tips();
        $(".btn_menu").removeClass("active");
        $(this).addClass("active");
        page_type = $("#page_type").val();
        generate_specific(page_type);
        getFiles('video');
        $(".directory_icon").css("background-image","url('ressources/imgs/videoDir_icon.png')");
    });
    $("#btn_music").click();
    
    $("#btn_yt").click(function(){
        $field = $("#ytField");
        if($field.val().length>0)
            stream($field.val());
    });
});


//Generate specific buttons in video/music/image playlist
function generate_specific(page_type){
    var btn_specific = $("#file_menu>#playlist_"+page_type+">div.form_specific>div.btn_listnormal>input.btn_specific");
    var btn_specificShuffle = $("#file_menu>#playlist_"+page_type+">div.form_specific>div.btn_listshuffle>input.btn_specific");
    var table_files = $("#file_container>.list_container>table.list>tbody");
    var table_playlist = $("#file_menu>#playlist_"+page_type+">.list_container>table.list>tbody");
    var playlist_content;
    
    btn_specific.unbind("click");
    btn_specific.click(function(){
        playlist_content = new Array();
        table_playlist.children("tr").each(function(index){
            playlist_content[index] = $(this).attr("path");
        });
        generate_switches();
        $("#btn_switchR").click();
        $("#remoteBtn_playpause").css("background-image","url('ressources/imgs/remoteBtn_pause.png')");
        $("#remoteBtn_playpause").attr("class","remoteBtn_pause");
        goplaylist(page_type,playlist_content);
    });
    
    btn_specificShuffle.unbind("click");
    btn_specificShuffle.click(function(){
        playlist_content = new Array();
        shuffle(table_playlist.children("tr"));
        table_playlist.children("tr").each(function(index){
            playlist_content[index] = $(this).attr("path");
        });
        generate_switches();
        $("#btn_switchR").click();
        $("#remoteBtn_playpause").css("background-image","url('ressources/imgs/remoteBtn_pause.png')");
        $("#remoteBtn_playpause").attr("class","remoteBtn_pause");
        goplaylist(page_type,playlist_content);
    });
    
    $("#btn_playAll>input.btn_specific").unbind("click");
    $("#btn_playAll>input.btn_specific").click(function(){
        table_playlist.empty();
        table_files.children("tr").children("td.cell_add").children("input.btn_addplaylist").each(function(){
            $(this).click();
        });
        btn_specific.click();
    });
    
    $("#btn_shuffleAll>input.btn_specific").unbind("click");
    $("#btn_shuffleAll>input.btn_specific").click(function(){
        table_playlist.empty();
        table_files.children("tr").children("td.cell_add").children("input.btn_addplaylist").each(function(){
            $(this).click();
        });
        shuffle(table_playlist.children("tr"));
        btn_specific.click();
    
    });
    
    $("#btn_addAll").unbind("click");
    $("#btn_addAll").click(function(){
        table_playlist.empty();
        table_files.children("tr").children("td.cell_add").children("input.btn_addplaylist").each(function(){
            $(this).click();
        });
    });


}

//Generate all buttons interaction between remote screen & playlist screen
function generate_btnInteraction(){
    var page_type = $("#page_type").val();
    var table_files = $("#file_container>.list_container>table.list>tbody");
    var playlist = $("#file_menu>#playlist_"+page_type);
    var btn_specific = $("#file_menu>#playlist_"+page_type+">div.form_specific>div.btn_listnormal>input.btn_specific");
    var table_playlist = $("#file_menu>#playlist_"+page_type+">.list_container>table.list>tbody");
    var table_playlistRemote = $("#remote_menu>.playlist_container>.list_container>table.list>tbody");
    var btn_empty = $("#file_menu>#playlist_"+page_type+">.list_head>table.head>thead>tr>th>input");
    var row, file_name, table_temp = "";
    var playlist_content;
    
    $(".btn_play").unbind("click");
    $(".btn_play").each(function(){
        $(this).click(function(){
            $(this).parent().next("td").children(".btn_addplaylist").click();
            generate_switches();
            $("#btn_switchR").click();
            $("#remoteBtn_playpause").css("background-image","url('ressources/imgs/remoteBtn_pause.png')");
            $("#remoteBtn_playpause").attr("class","remoteBtn_pause");
        });
    });
    
    $(".btn_addplaylist").unbind("click");
    $(".btn_addplaylist").each(function(){
        $(this).click(function(){
            if(playlist.css("display")=="none")
                playlist.css("display","block");
            file_name = $(this).parent().parent().children(".cell_name").text();
            tmp = $(this).parent().parent().children(".cell_play").children("input").attr("onclick");
            tmp = tmp.substr(6);
            file_path = tmp.substr(0, tmp.length-2);
            row_file = '<tr path='+'"'+file_path+'"'+'><td class="cell_name">'+file_name+'</td>';
            row_file += '<td class="cell_up"><input class="btn_up" type="button" value="" title="Up"></td>';
            row_file += '<td class="cell_down"><input class="btn_down" type="button" value="" title="Down"></td>';
            row_file += '<td class="cell_del"><input class="btn_del" type="button" value="" title="Delete"></td></tr>';
            table_playlist.append(row_file);
            generate_switches();
            generate_tips();
            generate_updownArrows();
            
            $('input.btn_up, input.btn_down').each(function(){
                $(this).unbind("click");
            });
            $('input.btn_up').click(function() {
                var parent = $(this).parent().parent();
                parent.insertBefore(parent.prev());
                generate_updownArrows();
            });
            $('input.btn_down').click(function() {
                var parent = $(this).parent().parent();
                parent.insertAfter(parent.next());
                generate_updownArrows();
            });
            
            $("input.btn_del").click(function(){
                index = $(this).parent().parent().parent().children().length;
                $(this).parent().parent().remove();
                $("#remote_menu>.playlist_container>.list_container>table.list>tbody tr:nth-child("+index+")").remove();
                if(table_playlist.children().length == 0){
                    playlist.css("display","none");
                    table_playlist.empty();
                }
                generate_updownArrows();
            });
        
        });
    });
    
    $("#remoteBtn_playpause").unbind("click");
    $("#remoteBtn_playpause").click(function(){
        if($(this).attr("class")=="remoteBtn_play"){
            $(this).css("background-image","url('ressources/imgs/remoteBtn_pause.png')");
            $(this).attr("class","remoteBtn_pause");
            if($("#remoteBtn_stop").hasClass("clicked")){
                btn_specific.click();
                $("#remoteBtn_stop").removeClass("clicked");
            }else{
            send(page_type, "pause");
            }
        }else if($(this).attr("class")=="remoteBtn_pause"){
            $(this).css("background-image","url('ressources/imgs/remoteBtn_play.png')");
            $(this).attr("class","remoteBtn_play");
            send(page_type, "pause");
        }
    });
    
    $("#remoteBtn_stop").unbind("click");
    $("#remoteBtn_stop").click(function(){
        $("#remoteBtn_playpause").css("background-image","url('ressources/imgs/remoteBtn_play.png')");
        $("#remoteBtn_playpause").attr("class","remoteBtn_play");
        $(this).addClass("clicked");
        send(page_type, "stop");
    });
    
    $("#remoteBtn_previous").unbind("click");
    $("#remoteBtn_previous").click(function(){
        send(page_type, "previous");
    });
    $("#remoteBtn_next").unbind("click");
    $("#remoteBtn_next").click(function(){
        send(page_type, "next");
    });
    
    btn_empty.unbind("click");
    btn_empty.click(function(){
        table_playlist.empty();
        playlist.css("display","none");
    });
    
    
    $("#q").unbind("keyup");
    $("#q").keyup(function(){
        $field = $(this);
        if($field.val().length>1)
            searchFiles(page_type, $field.val());
        else getFiles(page_type);
    });
    
    $("#btn_import").unbind("click");
    $("#btn_import").click(function(){
        $field = $("#importField");
        if($field.val().length>0)
            mountFolder(page_type, $field.val());
    });
    
    if(page_type=="image"){
        $("td.cell_play>input").each(function(){
            $(this).attr("class","btn_watchPhoto");
            $(this).attr("title","Show");
        });
    }else{
        $("td.cell_play>input").each(function(){
            $(this).attr("class","btn_play");
            $(this).attr("title","Play");
        });
    }
}

//Generate the arrows actions
function generate_updownArrows(){
    var page_type = $("#page_type").val();
    var table_playlist = $("#file_menu>#playlist_"+page_type+">.list_container>table.list>tbody");
    $("input.btn_down, input.btn_up").css("display","block");
    table_playlist.children("tr:first").children("td.cell_up").children("input.btn_up").css("display","none");
    table_playlist.children("tr:last").children("td.cell_down").children("input.btn_down").css("display","none");
}

//Generate the switches actions
function generate_switches(){
    var page_type = $("#page_type").val();
    var playlist = $("#file_menu>#playlist_"+page_type);
    var table_playlist = $("#file_menu>#playlist_"+page_type+">.list_container>table.list>tbody");
    var table_playlistRemote = $("#remote_menu>.playlist_container>.list_container>table.list>tbody");
    
    $("#btn_switchR").unbind("click");
    $("#btn_switchR").click(function(){
        table_temp = table_playlist.clone();
        table_temp.children("tr").children("td.cell_del").remove();
        table_temp.children("tr").children("td.cell_up").remove();
        table_temp.children("tr").children("td.cell_down").remove();
        table_playlistRemote.replaceWith(table_temp);
        $("#remote_menu").css("display","block");
        $("#file_menu").css("display","none");
        $("#remote_menu").css("float","left");
        $("#file_menu").css("float","right");
        generate_tips();
    });
    
    $("#btn_switchL").unbind("click");
    $("#btn_switchL").click(function(){
        $("#remote_menu").css("display","none");
        $("#file_menu").css("display","block");
        $("#remote_menu").css("float","right");
        $("#file_menu").css("float","left");
        generate_tips();
    });
}

//Generate all the tips
function generate_tips(){
    var tip = $("#tip");
    var elmts = $("*[title]");
    
    elmts.unbind("mouseover");
    elmts.unbind("mouseout");
    elmts.unbind("mousemove");
    elmts.each( function(){
        $(this).mouseover( function(){
            $(this).data("aide", $(this).attr("title"));
            $(this).removeAttr("title");
            tip.html($(this).data("aide"));
            tip.fadeIn(50);
        })
        $(this).mouseout( function(){
            tip.fadeOut(0);
        })
        $(this).mousemove( function(event){
            tip.css("left", event.pageX-20);
            tip.css("top", event.pageY+15);
        })
    });
}

//Function to shuffle the playlist
function shuffle(table_rows){
    var $Rows = table_rows;        
    $copies = $Rows.clone(true);            
    [].sort.call($copies, function() {
        return Math.random() - 0.5;
    });            
    $copies.each(function(i){          
        $Rows.eq(i).replaceWith(this);      
    });
}

//Function special switch for youtube video
function yt_switchR(data){
    var table_playlistRemote = $("#remote_menu>.playlist_container>.list_container>table.list>tbody");
    table_playlistRemote.empty();
    row = "<tr><td>"+data[0].name+"</td></tr>";
    table_playlistRemote.append(row);
    $("#remote_menu").css("display","block");
    $("#file_menu").css("display","none");
    $("#remote_menu").css("float","left");
    $("#file_menu").css("float","right");
    generate_tips();
}

//Function to display the files on the file list
function displayFiles(data){
    //new folder incoming, fetch the table and flush it
    var table_files = $("#file_menu>#file_container>.list_container>.list>tbody");
    table_files.empty();
    
    // check if it is object-type JSON
    var valTest = data.length ;
    if(! isNaN(valTest))
    {
        // prepare a new row to display on the table
        var row = null;
        var canBack = false;
        
        var fileArray =  new Array();
        var dirArray = new Array();
        
        $.each(data, function(index, element){
            var icon = null;
            var name = null;
            var path = null;
            var play = null;
            var addplaylist = null;
            path = escape(element.path);
            if(element.name != "..")
                name = '<td class="cell_name">'+element.name+'</td>';
            if(element.isDirectory)
            {   
                if(element.name == ".."){
                   $("#btn_back").css("display", "block");
                   $(".separator").css("display", "block");
                   $("#btn_back").attr("onclick", "getFiles(\'"+(path)+"\')");
                   canBack = true;
                }else{
                    row = $('<tr onclick="getFiles(\''+(path)+'\')"></tr>');
                    icon ='<td class="cell_icon"><div class="directory_icon"></div></td>';
                    play = '<td></td>';
                    addplaylist = '<td></td>';
                }
            }
            else
            {
                row = $("<tr></tr>");
                icon ='<td class="cell_icon" ><div class="'+ $("#page_type").val() +'_icon"></div></td>';
                play = '<td class="cell_play"><input type="button" title="play" value="" onclick="play(\''+element.path+'\')" class="btn_play"></td>';
                
                addplaylist = '<td class="cell_add"><input type="button" value="" title ="add to playlist" class="btn_addplaylist"></td>';
            }
            
            if(element.name != ".."){
                // insert element in the row
                row.append(icon);
                row.append(name);
                row.append(play);
                row.append(addplaylist);

                if(element.isDirectory)
                    dirArray.push(row);
                else
                    fileArray.push(row);
            }
        
        });
    }
    else
        console.log('invalid JSON');
    
    if(canBack==false){
        $("#btn_back").css("display", "none");
        $(".first").css("display", "none");
    }

    if($("#importField").css("display")=="none")
        $(".second").css("display", "none");
    
    // insert the row in the table
    for(var i=0;i<dirArray.length;i++)
        table_files.append(dirArray[i]);
    for(i=0;i<fileArray.length;i++)
        table_files.append(fileArray[i]);
    
    $(".directory_icon").css("background","url('ressources/imgs/"+$("#page_type").val()+"Dir_icon.png') no-repeat");
    generate_tips();
    generate_btnInteraction();
}
