<?php

namespace Util;

/**
 * Description of AjaxUtil
 *
 * @author Etienne Bossuet <etienne.bossuet@etu-u-bordeaux1.fr>
 * @author Charlie Maugin <charlie.maugin@etu.u-bordeaux1.fr>
 * @author Hugo Quezada Serres <hugo.quezada--serres@etu.u-bordeaux1.fr>
 */
class AjaxUtil
{

    /**
     * list of JSON objects to send in the response 
     */
    private static $_params = array();

    /**
     * getter on the list of parameters
     * @return array array with all parametters into the response
     */
    public static function getParams()
    {
        return self::_params;
    }

    /**
     * add a parameter to the response
     */
    public static function addParam($param)
    {
        self::$_params[] = $param;
    }
    
    /**
     * send the response and flush all existing parameters
     */
    public static function send()
    {
        header('Content-Type: application/json');
        $json = json_encode(self::$_params);
        echo $json;
        self::$_params = array();
    }

}

?>
