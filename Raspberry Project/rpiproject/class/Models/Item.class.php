<?php
namespace Models;
use Models\Interfaces\OMXPlayable;
use Models\Interfaces\FBIPlayable;
/**
 * Abstract object which will represent any file on the raspberry
 *
 * @author Etienne Bossuet <etienne.bossuet@etu-u-bordeaux1.fr>
 * @author Charlie Maugin <charlie.maugin@etu.u-bordeaux1.fr>
 * @author Hugo Quezada Serres <hugo.quezada--serres@etu.u-bordeaux1.fr>
 */
class Item  implements OMXPlayable, FBIPlayable
{
    protected $_absolutePath;
	
	public function __construct($path)
    {
        $this->_absolutePath = $path;
    }

    public function getPath()
    {
        return $this->_absolutePath;
    }
}

?>
